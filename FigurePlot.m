% plot the colored figures of one PFA and one N and corresponding immediate reward
clc;
close all
clear all
load 'testreward_parameters1102020_8_18_12_33_.mat' %load one file from RL_MC
fout2= fout{1}
N_grid=fin.N_grid;
Tmax=fin.Tmax;
nu1 = -0.4;
nu2 = -0.2;
nu3 = 0;
nu4 =  0.1;
nu5 = 0.3;
nu6 = 0.35;
p(1) = 0.5*exp(1j*2*pi*nu1);
p(2) = 0.6*exp(1j*2*pi*nu2);
p(3) = 0.7*exp(1j*2*pi*nu3);
p(4) = 0.4*exp(1j*2*pi*nu4);
p(5) = 0.5*exp(1j*2*pi*nu5);
p(6) = 0.6*exp(1j*2*pi*nu6);
% nu1 = 0;
% nu2 = -0.1;
% nu3 = 0.01;
% p(1) = 0.5*exp(1j*2*pi*nu1);
% p(2) = 0.3*exp(1j*2*pi*nu2);
% p(3) = 0.4*exp(1j*2*pi*nu3);
 rho = - poly(p);
 plot_normalized_PSD(rho); %plot clutter

% 
% plot detection results

GLRT_out_tmp=zeros(length(N_grid),Tmax);
Q_value_outlook_diff_tmp=zeros(1,Tmax-2);
reward=zeros(1,Tmax);
Q_value_outlook=zeros(1,Tmax);
GLRT_out_tmporth=zeros(length(N_grid),Tmax);
PFA=zeros(length(N_grid),Tmax);
    PFA_orth=zeros(length(N_grid),Tmax);
for ii=1:Tmax
    for tt=1:fin.MC_Num
        fout_tmp=fout{tt};
        fout_tmporth=foutorth{tt};
        GLRT_out_tmp(:,ii) =GLRT_out_tmp(:,ii)+ fout_tmp.GLRT_out(:,ii);
        GLRT_out_tmporth(:,ii) =GLRT_out_tmporth(:,ii)+ fout_tmporth.GLRT_out(:,ii);
        reward(ii)=reward(ii)+fout_tmp.reward(ii);
       reward(ii)=reward(ii)+fout_tmp.reward(ii);
        Q_value_outlook(ii)=Q_value_outlook(ii)+max(max(fout_tmp.Q_value_outlook{ii}));
             PFA(:,ii) =PFA(:,ii)+ fout_tmp.PFA(:,ii);
            PFA_orth(:,ii) =PFA_orth(:,ii)+ fout_tmporth.PFA(:,ii);
       if ii< Tmax-1
              Q_value_outlook_diff_tmp(ii)=Q_value_outlook_diff_tmp(ii)+fout_tmp.Q_value_outlook_diff_norm(ii);
       end

    end
    Q_value_outlook(:,ii)=Q_value_outlook(:,ii)/fin.MC_Num;
        GLRT_out_tmp(:,ii)=GLRT_out_tmp(:,ii)/fin.MC_Num;
    reward(ii)=reward(ii)/fin.MC_Num;
    GLRT_out_tmporth(:,ii)=GLRT_out_tmporth(:,ii)/fin.MC_Num;
     PFA(:,ii)=PFA(:,ii)/fin.MC_Num;
        PFA_orth(:,ii)=PFA_orth(:,ii)/fin.MC_Num;
    if ii< Tmax-1
     Q_value_outlook_diff_tmp(ii)= Q_value_outlook_diff_tmp(ii)/fin.MC_Num;
    end
end
max_RL=max(max(GLRT_out_tmp));
min_RL=min(min(GLRT_out_tmp));
max_orth=max(max(GLRT_out_tmporth));
min_orth=min(min(GLRT_out_tmporth));
max_lim=max(max_RL,max_orth);
min_lim=min(min_RL,min_orth);

figure
plot(Q_value_outlook_diff_tmp)
title('Qdiff')
figure
plot(reward)
title('immediate reward')
figure
plot(Q_value_outlook)
title('maxQval')
lim=[min_lim max_lim];
figure
set(gca,'Position',[.08 .11 .86 .83]);  
set(gca,'fontsize',10.5)
imagesc(GLRT_out_tmp); 
shading interp
caxis(lim)
colorbar
title('detection for RL')

figure
imagesc(GLRT_out_tmporth);
shading interp
colorbar
caxis(lim)
title('detection for Orthogonal')

% xlabel('time')
% ylabel('angle')
hold off
rho = - poly(p);

BP_tmp = BP./max(BP) ;
BP_tmp = 10*log10(BP_tmp);
figure
plot_normalized_PSD(rho);
hold on
plot(N_grid,BP_tmp(:,35))
hold on
plot(N_grid,BP_tmp(:,5))
hold on
figure 
set(gca,'Position',[.08 .11 .86 .83]);  
set(gca,'fontsize',10.5)
imagesc(BP_tmp); hold on 
title('The beam pattern of optimization of beamforming with SARSA algorithm')
colormap((gray))
colorbar

