function RL_MC(fin) %This function loops over MC


fin.eta = fin.lambda/(fin.lambda-1);


Time = clock;
%%
BP=zeros(size(fin.N_grid,fin.Tmax));
if fin.roccurve==0
    loop=length(fin.Ntvect);
else
    loop=length(fin.Pfvect);
end
%% loop over number of antennas or PFA
%% hint:  To draw ROC curve for Nt=10, set fin.Pf=fin.Pfvect(n); fin.Nr=fin.Nt(10) ;
for n=1:loop %% draw PD vs N
    if fin.roccurve==0
        fin.Pf=fin.Pfvect(2); %use PFA=10^-4 here
        fin.Nt = fin.Ntvect(n);
    else
        fin.Pf=fin.Pfvect(n); 
        fin.Nt = fin.Ntvect(10);   %use Nt=10 here
    end
    
    fin.Nr=fin.Nt ;
    %% loop over MC runs
    for ii = 1:fin.MC_Num
        fout{ii}= RL_main_static(fin,0); % call the main file for RL radar
        fout_tmp= fout{ii};
        BP=BP+fout_tmp.BP; %Beampattern
        foutorth{ii}= RL_main_static(fin,1);  %call the main file for Omnidirectional radar
        disp(['montecarlo_',num2str(ii)]);
    end
    
    BP=BP/fin.MC_Num;
    save(['RL',num2str(fin.MC_Num),num2str(n),num2str(Time(1)),'_',num2str(Time(2)),'_',num2str(Time(3)),'_',num2str(Time(4)),...
        '_',num2str(Time(5)),'_.mat']);
    
end
end