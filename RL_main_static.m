function fout = RL_main_static(fin,orthogonal)
Tmax = fin.Tmax;
N_grid = fin.N_grid;
Nt = fin.Nt;
Nr = fin.Nr;
% alfa_t = fin.alfa_t;
timechangePAR= fin.timechangePAR;

lambda=fin.lambda;
pp=fin.p;
eta=fin.eta;
sigma2_c=fin.sigma2_c;
scale=eta/lambda;
theta_grid=fin.theta_grid;
Pw = fin.Pw ;
Pn = fin.Pn ;
MaxTarAngNum = fin.MaxTarAngNum ;
Th = chi2inv(1-fin.Pf,2);
Cm_1 = [];
Q_value_outlook_diff_norm = [] ;
optgvar = [] ;
optgvar.Qvalue = zeros(MaxTarAngNum+1,MaxTarAngNum+1);
optgvar.state_last = 1;
optgvar.action_last = 1;
M = Nt*Nr ;
ll=ceil(M^(1/4));
%Decaying epsilon
decay = 0.999;
min_epsilon = 0.1;
epsilon=fin.epsilon;
%
target_matrix=zeros(length(N_grid),1);

GLRT_out = zeros(length(N_grid),Tmax);

Alpha_all = zeros(length(N_grid),Tmax);
GLRT_StaValue_all = zeros(length(N_grid),Tmax);
PFA_W_T=zeros(length(N_grid),Tmax);
BP = zeros(length(N_grid),Tmax);
A_Sample = zeros(Nt,length(N_grid));
Cm_1 = sqrt((Pw/Nt))*eye(Nt) ;
%%
%Map the SNR to each cell in the grid based if the scenario is static or
%dynamic
if timechangePAR==1 || timechangePAR==3 || timechangePAR==5
    timechange=fin.timechange;

for uu=1:size(theta_grid,2) %time changes
            tmp=theta_grid{uu};
            tmpsnr=fin.SNR{uu};  

%     for ii=1:size(theta_grid,2)
if isempty(tmp)
    target_matrix(:,uu)=0;
    alfa_t(:,uu)=0;
else 
    for ii=1:size(tmp,2) %angles
%       target_matrix(theta_grid(uu,ii),uu)=alfa_t(ii);
    alfa_t(ii,uu)=sqrt(power(10,tmpsnr(ii)/10));  %just absolute here
    target_matrix(tmp(ii),uu)=alfa_t(ii,uu);
    end
end
end
uu=1;
else
    alfa_t=sqrt(power(10,fin.SNR/10));
    for ii=1:length(theta_grid)
                target_matrix(theta_grid(ii))=alfa_t(ii);
     end 
end
 %% loop over time K
for tt=1:Tmax
    flag=0;
    if timechangePAR==1

     if timechange(tt)==1
     uu=uu+1;
     end
    end
        %% Loop over cells of targets
        for l=1:length(N_grid)
            A_Sample = exp(1j*2*pi.*[0:Nt-1]*(N_grid(l))).'; % steering vector
            h_vec= kron(Cm_1.'*A_Sample,A_Sample); %supposed to be NTNRx1
            if flag == 0
                noise=AR_gen_t_dist(M,pp,sigma2_c,lambda,scale); %generate clutter based on [5]
                Y_vec=noise; %H0 in eq.(8)
             if timechangePAR==1 || timechangePAR==5
                trgt_pos=(target_matrix(l,uu));
                elseif timechangePAR==3
                trgt_pos=(target_matrix(l,tt));

                else
                trgt_pos=(target_matrix(l));

                end
         %      if find(target_matrix(l))~= 0
                    
               if trgt_pos~= 0 %changing angles
%                     if tt >51
%                         uu=2;%changine angle
%                     else 
%                         uu=1;
                %                     end
                if timechangePAR==1 || timechangePAR==5

                    tgt_coef =target_matrix(l,uu).*exp(1j*2*pi*rand(1));
                elseif timechangePAR==3
                    tgt_coef =target_matrix(l,tt).*exp(1j*2*pi*rand(1));
                else
                    tgt_coef =target_matrix(l).*exp(1j*2*pi*rand(1));
                end
                    Y_vec= Y_vec+tgt_coef.*h_vec;
                end
            end
            n2 = (h_vec'*h_vec);
            
            % Estimation of the signal parameter alpha
            hat_alpha = (h_vec'*Y_vec)/n2; %\alpha^k_l in eq.(11)
            
            % Wald Test
            StaValue = Wald_test(M,h_vec,Y_vec,hat_alpha,ll,n2); %detector in eq.(12)
            
            if StaValue < 0
                StaValue=eps;
            end
            %         
            GLRT_StaValue_all(l,tt) = StaValue ;
            PFA_W_T(l,tt)=(sign(StaValue-Th)+1)/2; %PFA
            
            
            if StaValue > Th
                GLRT_out(l,tt) = 1; %eq.(18)
            end
            Alpha_all(l,tt) = h_vec'* Y_vec/real(h_vec'*h_vec);  %\alpha^k_l in eq.(11)
            
        end
        %% SARSA Parameters and call
       

%         epsilon = max(min_epsilon, epsilon*decay);
        Opt_fin.GLRT_out_tt = GLRT_out(:,tt);
        Opt_fin.GLRT_StaValue_all=GLRT_StaValue_all(:,tt);
        Opt_fin.Th=Th;
        Opt_fin.Alpha_tt = Alpha_all(:,tt);
        Opt_fin.Nt = Nt ;
        Opt_fin.Pw = Pw ;
        Opt_fin.max_angel_num = MaxTarAngNum ;
        Opt_fin.N_grid = N_grid ;
        Opt_fin.epsilon = fin.epsilon  ;
        Opt_fin.alfa = fin.alfa;
        Opt_fin.gamma = fin.gamma  ;
        Opt_fin.optgvar = optgvar;
        if orthogonal==1 %if radar is orthogonal just send equal power for all antennas
            Cm_1 = sqrt((Pw/Nt))*eye(Nt) ;
             Obj{tt}=0;
        else
            [Cm_1,Para_outlook] = SARSA(Opt_fin); %SARSA call per time k
           
            Q_value_outlook{tt} = Para_outlook.Q_value_outlook ; %Q matrix
            reward(tt)=Para_outlook.reward;
            optgvar = Para_outlook.optgvar ; %Optimum beampatterns
            Obj{tt}=Para_outlook.Obj;
            if tt>2
                Q_value_outlook_diff_norm = [Q_value_outlook_diff_norm norm(Q_value_outlook{tt}-Q_value_outlook{tt-1})];
            end
        end
        flag=1;
   
    
    
    %% beam shape
    for ii=1:length(N_grid)
        A_Sample = exp(1j*2*pi.*[0:Nt-1]*(N_grid(ii))).';
        BP(ii,tt) = real(A_Sample.'*Cm_1*Cm_1'*(conj(A_Sample))) ;
    end
    %   plot( N_grid,(BP(:,tt)/max(BP(:,tt))));
    % hold on
    
end
%
fout.GLRT_out = GLRT_out;
fout.GLRT_StaValue = GLRT_StaValue_all ;
if orthogonal~=1
    fout.Q_value_outlook = Q_value_outlook ;
    fout.Q_value_outlook_diff_norm = Q_value_outlook_diff_norm ;
    fout.reward=reward;
end
fout.BP = BP ;
fout.alfa_t = alfa_t;
fout.Th=Th;
fout.PFA=PFA_W_T;
fout.Obj=Obj;
end
