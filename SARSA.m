function [C_opt,Para_outlook] = SARSA(para)

GLRT_out_tt = para.GLRT_out_tt ; %equation 15
GLRT_StaValue_all=para.GLRT_StaValue_all;
Th=para.Th;
Nt = para.Nt;
Pw = para.Pw ;
max_angel_num = para.max_angel_num ; %maximum angles
N_grid = para.N_grid;

epsilon = para.epsilon  ;
alfa = para.alfa  ;
gamma = para.gamma  ;

optgvar = para.optgvar;

Qvalue = optgvar.Qvalue ;
state_last = optgvar.state_last ;
action_last = optgvar.action_last ;

%% reward
stat_pd=marcumq(sqrt(GLRT_StaValue_all),sqrt(Th)); %Theoritical PD in eq.(21)
GLRT_out_tt_pfa=GLRT_out_tt+1;
GLRT_out_tt_pfa(GLRT_out_tt_pfa==2)=0;
GLRT_out_tt_theta = sum(GLRT_out_tt); %State in eq.(19)
reward = sum((stat_pd.*GLRT_out_tt))-sum((stat_pd.*GLRT_out_tt_pfa)); %reward in eq.(23)


%% Updata the Q matrix value
% the number of angel detected targets
if GLRT_out_tt_theta>max_angel_num
    state_tmp = max_angel_num + 1;   % because the 0 state is the first state % this is equation 16
else
    state_tmp = GLRT_out_tt_theta+ 1 ;
end

[action_out] = epsilongreedy(epsilon,state_tmp,Qvalue); %epsilon greedy policy explained in section 3.E


Qvalue(state_last,action_last) = Qvalue(state_last,action_last)+ alfa*(reward+gamma...
    *Qvalue(state_tmp,action_out)-Qvalue(state_last,action_last)); %Update Q function in eq.(17)

state_last = state_tmp ; %set s_{k+1}
action_last = action_out ; %set a_{k+1}
%%
Wagveformfin = [] ;
if action_out==1 %if No angles detected set W=I
    C_opt =sqrt((Pw/Nt))*eye(Nt) ;
    Wagveformfin.Theta = [];
     Obj=0;
else
    % action_out-1 is the number of direction of beamforming
    [~,StaValue_Id] = maxk((GLRT_StaValue_all.'),action_out-1) ; %take maximum across col of ranges and get which col has it
   
    Wagveformfin.Nt = Nt ;
    Wagveformfin.N = Nt ;
    Wagveformfin.Pt = Pw ;
    Wagveformfin.Theta = N_grid(StaValue_Id); %set of Theta_i
    [Obj,C_opt] = WaveformGenerator(Wagveformfin); %solve optimization problem in (20)
    
end
Para_outlook.Q_value_outlook = Qvalue ;
Para_outlook.reward=reward;
Para_outlook.optgvar.Qvalue = Qvalue ;
Para_outlook.optgvar.state_last = state_last ;
Para_outlook.optgvar.action_last = action_last ;
Para_outlook.Obj=Obj;

function [action_out] = epsilongreedy(epsilon,state_cur,Qvalue_in) %check the example in the paper for more details
[~, index] = max(Qvalue_in(state_cur,:));
if state_cur>1
    actionset_except = [state_cur-1:size(Qvalue_in,2)]; %set A of all actions that can be taken within this state and it refers to the maximum number of targets that we can detect
else
    actionset_except = [2:size(Qvalue_in,2)];
end
actionset_except = setdiff(actionset_except,index);

tmp = rand();
if tmp<epsilon % if 0 will always take a random action, if 1 will take determinate index
    action_out = randsample(actionset_except,1);
else
    action_out = index ;
end


