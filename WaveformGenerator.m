function [Obj,Cs] = WaveformGenerator(para)

Nt = para.Nt ;
Pt =para.Pt ;
N = para.N ;
Theta = para.Theta ;


 p = length(Theta) ;

    A = zeros(Nt,p); 
    for i=1:length(Theta)
           A(:,i) = exp(1j*2*pi*[0:Nt-1]*(Theta(i)));           
    end
%% Previous SDP solution in[24]
%     cvx_begin sdp %Previous SDP solution in[24]
%         cvx_quiet(true)
%         cvx_solver mosek
%         variable R(Nt,Nt) complex semidefinite
%         variable k
%         minimize -k
%         subject to 
%             for ii=1:p
%              real(trace(R*conj(A(:,ii))*A(:,ii).')) >= k
%   
%             end
%             trace(R) == Pt;
%             R == hermitian_semidefinite(Nt); 
%             k>=0 ;
%         cvx_end 
%     Cs = WavefoemWeightedMatrixSyn(R,Pt/Nt,N);
    Theta;
%% Algorithm 2 proposed
%  tic
[Obj,Cs] = Alg2v1(Nt,(A),Pt); 
% toc
%% Closed form Solution
%[Obj,Cs]=Closed_Form_W(A,Pt);
%% If you want to draw output beamformer uncomment the following 
% % ThetaSample = [-90:0.5:90].*pi./180 ;
%  ThetaSample=[-0.5:0.05:0.5-0.05];
% % 
% P_Theta = zeros(1,length(ThetaSample));
% A_Sample = zeros(Nt,length(ThetaSample)); 
% for i=1:length(ThetaSample)
%       A_Sample(:,i) = exp(1j*2*pi*[0:Nt-1]*(ThetaSample(i)));
%       
% end  
% for i=1:length(ThetaSample)
%     P_Theta(i) =real(A_Sample(:,i).'*Cs*Cs'*conj((A_Sample(:,i)))) ;
% % real(A_Sample(:,i)'*Cs*Cs'*(A_Sample(:,i)))
% % real((Cs(:))'*kron(eye(Nt),(A_Sample(:,i)*A_Sample(:,i)'))*Cs(:))
% % real(trace(conj(Cs*Cs'*A_Sample(:,i))*A_Sample(:,i).'))
% end
%% If you want to draw the respective clutter uncomnent the following
% % % % nu1 = -0.4;
% % % % nu2 = -0.2;
% % % % nu3 = 0;
% % % % nu4 =  0.1;
% % % % nu5 = 0.3;
% % % % nu6 = 0.35;
% % % % pc(1) = 0.5*exp(1j*2*pi*nu1);
% % % % pc(2) = 0.6*exp(1j*2*pi*nu2);
% % % % pc(3) = 0.7*exp(1j*2*pi*nu3);
% % % % pc(4) = 0.4*exp(1j*2*pi*nu4);
% % % % pc(5) = 0.5*exp(1j*2*pi*nu5);
% % % % pc(6) = 0.6*exp(1j*2*pi*nu6);
% % % % rho = - poly(pc);
% % % nu1 = 0;
% % % nu2 = -0.1;
% % % nu3 = 0.05;
% % % p(1) = 0.5*exp(1j*2*pi*nu1);
% % % p(2) = 0.3*exp(1j*2*pi*nu2);
% % % p(3) = 0.4*exp(1j*2*pi*nu3);
% % % rho = - poly(p);
% % % 
% % % Normalized PSD of the AR(p) disturbance
% % % plot_normalized_PSD(rho);
% % % hold on
% %  plot(ThetaSample,(P_Theta),'k-d','LineWidth',1);
end
