%% plot ROC curve or PD vs N curve in Fig.(4) and Fig. (5)
clc;
close all
 clear all
graph=0;
cntr=1;
Nt=floor(logspace(1,2,10)) %plot PD vs N
PFA_loop=[1e-5 1e-4 1e-3 1e-2 1e-1 1]; %plot PD vs PFA
f = [-0.5:0.05:0.5]; % \nu in eq.(33)
angles= [-0.2 0 0.2 0.3]; %cells that contain target
pos=[7 11 15 17]; %cells number
N=Nt.*Nt;
ROCcurve=1; % 0 for Fig. (4) and 1 for Fig.(5)
if ROCcurve==1
    loop=length(PFA_loop);
else
    loop=length(N)
end
for n=1:loop
    str=['RL1',num2str(n),'2020_12_1_13_4_.mat']; %change file name here that is saved in RL_MC
    load (str)
%load('Sim_result_100PFA_Ant_12019_10_2_13_21_.mat')
    fout2= fout{1}
    N_grid=fin.N_grid;
%     targets=[7 11 15 17];
    Tmax=fin.Tmax;
    nu1 = -0.4;
    nu2 = -0.2;
    nu3 = 0;
    nu4 =  0.1;
    nu5 = 0.3;
    nu6 = 0.35;
    p(1) = 0.5*exp(1j*2*pi*nu1);
    p(2) = 0.6*exp(1j*2*pi*nu2);
    p(3) = 0.7*exp(1j*2*pi*nu3);
    p(4) = 0.4*exp(1j*2*pi*nu4);
    p(5) = 0.5*exp(1j*2*pi*nu5);
    p(6) = 0.6*exp(1j*2*pi*nu6);
    % nu1 = 0;
    % nu2 = -0.1;
    % nu3 = 0.01;
    % p(1) = 0.5*exp(1j*2*pi*nu1);
    % p(2) = 0.3*exp(1j*2*pi*nu2);
    % p(3) = 0.4*exp(1j*2*pi*nu3);
    rho = - poly(p);
    
    %
    % plot detection results
    
    GLRT_out_tmp=zeros(length(N_grid),Tmax);
    Q_value_outlook_diff_tmp=zeros(1,Tmax-2);
    reward=zeros(1,Tmax);
    Q_value_outlook=zeros(1,Tmax);
    GLRT_out_tmporth=zeros(length(N_grid),Tmax);
    PFA=zeros(length(N_grid),Tmax);
    PFA_orth=zeros(length(N_grid),Tmax);
    
    for ii=1:Tmax %loop over time
        for tt=1:fin.MC_Num %average over MC
            fout_tmp=fout{tt};
            fout_tmporth=foutorth{tt};
            GLRT_out_tmp(:,ii) =GLRT_out_tmp(:,ii)+ fout_tmp.GLRT_out(:,ii);
            GLRT_out_tmporth(:,ii) =GLRT_out_tmporth(:,ii)+ fout_tmporth.GLRT_out(:,ii);
            reward(ii)=reward(ii)+fout_tmp.reward(ii);
            reward(ii)=reward(ii)+fout_tmp.reward(ii);
            Q_value_outlook(ii)=Q_value_outlook(ii)+max(max(fout_tmp.Q_value_outlook{ii}));
            PFA(:,ii) =PFA(:,ii)+ fout_tmp.PFA(:,ii);
            PFA_orth(:,ii) =PFA_orth(:,ii)+ fout_tmporth.PFA(:,ii);
            
            
            if ii< Tmax-1
                Q_value_outlook_diff_tmp(ii)=Q_value_outlook_diff_tmp(ii)+fout_tmp.Q_value_outlook_diff_norm(ii);
            end
            
        end
        Q_value_outlook(:,ii)=Q_value_outlook(:,ii)/fin.MC_Num;
        
        GLRT_out_tmp(:,ii)=GLRT_out_tmp(:,ii)/fin.MC_Num;
        reward(ii)=reward(ii)/fin.MC_Num;
        PFA(:,ii)=PFA(:,ii)/fin.MC_Num;
        PFA_orth(:,ii)=PFA_orth(:,ii)/fin.MC_Num;
        
        GLRT_out_tmporth(:,ii)=GLRT_out_tmporth(:,ii)/fin.MC_Num;
        if ii< Tmax-1
            Q_value_outlook_diff_tmp(ii)= Q_value_outlook_diff_tmp(ii)/fin.MC_Num;
        end
      
    end
   %% calculate the PD in the cells which has the target
    PFA_ant0(cntr)=mean(PFA(pos(1),:));
    PFA_antorth0(cntr)=mean(PFA_orth(pos(1),:));
    
    PFA_ant(cntr)=mean(PFA(pos(2),:));
    PFA_antorth(cntr)=mean(PFA_orth(pos(2),:));
    
     PFA_ant2(cntr)=mean(PFA(pos(3),:));
    PFA_antorth2(cntr)=mean(PFA_orth(pos(3),:));
    
      PFA_ant3(cntr)=mean(PFA(pos(4),:));
    PFA_antorth3(cntr)=mean(PFA_orth(pos(4),:));
%        PFA_ant4(cntr)=mean(PFA(19,:));
%     PFA_antorth4(cntr)=mean(PFA_orth(19,:));
    %%
    if graph==1
%        plot_normalized_PSD(rho);
        
        max_RL=max(max(GLRT_out_tmp));
        min_RL=min(min(GLRT_out_tmp));
        max_orth=max(max(GLRT_out_tmporth));
        min_orth=min(min(GLRT_out_tmporth));
        max_lim=max(max_RL,max_orth);
        min_lim=min(min_RL,min_orth);
        figure
        plot(mean(PFA(11,:)))
        hold on
        plot(mean(PFA_orth(11,:)))
%         
%         figure
%         plot(Q_value_outlook_diff_tmp)
%         title('Qdiff')
        figure
        plot(reward)
        title('reward')
        figure
        plot(Q_value_outlook)
        title('maxQval')
        lim=[min_lim max_lim];
        
        set(gca,'Position',[.08 .11 .86 .83]);
        set(gca,'fontsize',10.5)
        imagesc(GLRT_out_tmp);
        shading interp
        caxis(lim)
        colorbar
                xlabel('Time steps')

              ylabel('Angle index')
        figure
        imagesc(GLRT_out_tmporth);
        shading interp
        colorbar
        caxis(lim)
                      ylabel('Angle index')

        xlabel('Time steps')
        title('detection for Orthogonal')
        
        % xlabel('time')
        % ylabel('angle')
        hold off
        % axis([1 100 1 20])
        % zlabel('time')
        % %%%%%%%
        rho = - poly(p);
        
        BP_tmp = BP./max(BP) ;
        BP_tmp = 10*log10(BP_tmp);
       
    end
    cntr=cntr+1;
end
if ROCcurve==0
figure %Fig.(4)
% subplot(6,1,1)
semilogx(N,PFA_antorth0,'-+')
hold on
semilogx(N,PFA_ant0,'r')
xlabel('Spatial Channels N')
ylabel('Pd')
grid on
legend('orth','RL')
title(['Target at \nu=',num2str(angles(1))])
%  subplot(6,1,2)
figure
semilogx(N,PFA_antorth,'-+')
grid on

hold on
 semilogx(N,PFA_ant,'r')
title(['Target at \nu=',num2str(angles(2))])
xlabel('Spatial Channels N')
ylabel('Pd')
grid on

legend('orth','RL')

%  subplot(6,1,3)
figure
semilogx(N,PFA_antorth2,'-+')
hold on
semilogx(N,PFA_ant2,'r')
grid on

title(['Target at \nu=',num2str(angles(3))])
xlabel('Spatial Channels N')
ylabel('Pd')
legend('orth','RL')

%  subplot(6,1,4)
figure
semilogx(N,PFA_antorth3,'-+')
hold on
semilogx(N,PFA_ant3,'r')
grid on
xlabel('Spatial Channels N')
ylabel('Pd')
title(['Target at \nu=',num2str(angles(4))])
legend('orth','RL')
else
    figure %Fig.(5)
% subplot(6,1,1)
semilogx(PFA_loop,PFA_antorth0,'-+')
hold on
semilogx(PFA_loop,PFA_ant0,'r')
xlabel('PFA')
ylabel('Pd')
grid on
legend('orth','RL')
title(['Target at \nu=',num2str(angles(1))])
%  subplot(6,1,2)
figure
semilogx(PFA_loop,PFA_antorth,'-+')
grid on

hold on
 semilogx(PFA_loop,PFA_ant,'r')
title(['Target at \nu=',num2str(angles(2))])
xlabel('PFA')
ylabel('Pd')
grid on

legend('orth','RL')

%  subplot(6,1,3)
figure
semilogx(PFA_loop,PFA_antorth2,'-+')
hold on
semilogx(PFA_loop,PFA_ant2,'r')
grid on

title(['Target at \nu=',num2str(angles(3))])
xlabel('PFA')
ylabel('Pd')
legend('orth','RL')

%  subplot(6,1,4)
figure
semilogx(PFA_loop,PFA_antorth3,'-+')
hold on
semilogx(PFA_loop,PFA_ant3,'r')
grid on
xlabel('PFA')
ylabel('Pd')
title(['Target at \nu=',num2str(angles(4))])
legend('orth','RL')
end
%  subplot(6,1,5)
% 
% semilogx(b,PFA_antorth4,'-+')
% hold on
% semilogx(b,PFA_ant4)
% 
% title(['angle',num2str(angles(5))])
% xlabel('PFA')
% ylabel('Pd')
% legend('orth','RL')
% subplot(6,1,5)
% S_norm=plot_normalized_PSD(rho);
% grid on
% 
% plot(f,(S_norm))
% xlabel('PFA')
% ylabel('Pd')
% grid on
% title('Normalized PSD')
