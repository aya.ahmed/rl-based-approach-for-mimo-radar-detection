%% This is the mail file to run
%% Those files are copy righted to Aya Mostafa Ahmed
%% (email: aya.mostafaibrahimahmad@rub.de)
%% Please refer to our paper for more details
tic
clear all; close all;
clc;
fin.Pfvect = [1e-5 1e-4 1e-3 1e-2 1e-1 1]; %Probability of false alarm array
fin.epsilon = 0.5 ; fin.alfa = 0.8; fin.gamma = 0.8; % SARSA algorithm parameters
 fin.Ntvect= floor(logspace(1,2,10)); % Number of transmit antennas array
fin.N_grid=[-0.5:0.05:0.5-0.05]; % L=20 angle cells, \nu as defined in eq.(33)
fin.timechangePAR=2; %scenario to be simulated
if fin.timechangePAR==1 %Dynamic scenario in Fig.10
        fin.Tmax = 60; %

    fin.theta_grid={[7,11],[],[7 11 14 16 17],[11,15],[10 12 16 18]}; %change angle positions
    fin.SNR = {[-5,-9],[],[-6 -8 -10 -11 -12],[-9,-8],[-8 -7 -10 -13]}; %changed SNR 
    time=zeros(1,fin.Tmax);
    timeIntervals=[11,21,36,51]; %time intervals where the environment changes
	time(timeIntervals)=1;
    fin.timechange=time;
else %Static scenario 
    fin.angles= [-0.2 0 0.2 0.3];%;-0.05 0.05 0.25 0.35];
    fin.theta_grid=[7 9 15 17];%10 12 16 18];[7 11 15 17];
    fin.SNR = [-5,-8, -10, -9]; %changed SNR
    fin.Tmax = 50; %
end

 fin.Pw = 1%Transmit poswer
fin.Pn = 1 ;
fin.MaxTarAngNum = 10 ; % M: maximum number of targets detected
%first clutter model, the below are clutter parameters
% Parameters of the t-distributed innovations in eq.(38) AR(6)

nu1 = -0.4;
nu2 = -0.2;
nu3 = 0;
nu4 =  0.1;
nu5 = 0.3;
nu6 = 0.35;
p(1) = 0.5*exp(1j*2*pi*nu1);
p(2) = 0.6*exp(1j*2*pi*nu2);
p(3) = 0.7*exp(1j*2*pi*nu3);
p(4) = 0.4*exp(1j*2*pi*nu4);
p(5) = 0.5*exp(1j*2*pi*nu5);
p(6) = 0.6*exp(1j*2*pi*nu6);
%second clutter model
% nu1 = 0;
% nu2 = -0.1;
% nu3 = 0.01;
% p(1) = 0.5*exp(1j*2*pi*nu1);
% p(2) = 0.3*exp(1j*2*pi*nu2);
% p(3) = 0.4*exp(1j*2*pi*nu3);
% nu1=0;
%  p(1) = 0.93*exp(1j*2*pi*nu1);
fin.p=p;
 fin.rho = - poly(p);
fin.sigma2_c = 1; %\sigma_w in section 5.B
fin.lambda = 2; %Clutter shape parameter in section 5.B
%S_norm=plot_normalized_PSD( fin.rho);

% number of Monte Calor runs
fin.MC_Num = 1;
fin.roccurve=1;
RL_MC(fin); %fin is an object with all the radar parameters
%% After this file finished running, please run  Figureplot and figure_plotloop with the updated file names as defined in 
%% RL_MC
 toc
beep